# Timecode generator

## What is it?
When you are writing documentation or taking notes from a class, often you want to timestamp when something important has happened. For me, often this means I'll do a pass marking things, Then review them later on. 

Each click of the button will add a new timestamp, relative to when the app first started.

See a demo of it at https://opposite-route.surge.sh